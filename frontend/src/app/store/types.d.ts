import { User } from '../models/user.model';
import { Task } from '../models/task.model';

export type UserState = {
  users: User[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string
};

export type TaskState = {
  tasks: Task[],
  fetchLoading: boolean,
  fetchError: null | string,
  createLoading: boolean,
  createError: null | string,
  editLoading: boolean,
  editError: null | string,
  deleteLoading: boolean,
  deleteError: null | string
};



export type AppState = {
  users: UserState,
  tasks: TaskState,
}
