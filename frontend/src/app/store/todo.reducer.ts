import { TaskState, UserState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createTaskFailure,
  createTaskRequest,
  createTaskSuccess,
  createUserFailure,
  createUserRequest,
  createUserSuccess,
  deleteTaskFailure,
  deleteTaskRequest,
  deleteTaskSuccess,
  editTaskFailure,
  editTaskRequest,
  editTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess,
  fetchUsersFailure,
  fetchUsersRequest,
  fetchUsersSuccess
} from './todo.actions';


const initialState: UserState = {
  users: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null
};

const initialStateTasks: TaskState = {
  tasks: [],
  fetchLoading: false,
  fetchError: null,
  createLoading: false,
  createError: null,
  editLoading: false,
  editError: null,
  deleteLoading: false,
  deleteError: null,
}

export const usersReducer = createReducer(
  initialState,
  on(fetchUsersRequest, state => ({...state, fetchLoading: true})),
  on(fetchUsersSuccess, (state, {users}) =>
    ({...state, fetchLoading: false, users})),
  on(fetchUsersFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),

  on(createUserRequest, state => ({...state, createLoading: true})),
  on(createUserSuccess, state => ({...state, createLoading: false})),
  on(createUserFailure, (state, {error}) =>
    ({...state, createLoading: false, createError: error}))
);

export const tasksReducer = createReducer(
  initialStateTasks,

  on(fetchTasksRequest, state => ({...state, fetchLoading: true})),
  on(fetchTasksSuccess, (state, {tasks}) =>
    ({...state, fetchLoading: false, tasks})),
  on(fetchTasksFailure, (state, {error}) =>
    ({...state, fetchLoading: false, fetchError: error})),

  on(createTaskRequest, state => ({...state, createLoading: true})),
  on(createTaskSuccess, state => ({...state, createLoading: false})),
  on(createTaskFailure, (state, {error}) =>
    ({...state, createLoading: false, createError: error})),


  on(editTaskRequest, state => ({...state, editLoading: true})),
  on(editTaskSuccess, state => ({...state, editLoading: false})),
  on(editTaskFailure, (state, {error}) =>
    ({...state, editLoading: false, editError: error})),


  on(deleteTaskRequest, state => ({...state, editLoading: true})),
  on(deleteTaskSuccess, state => ({...state, editLoading: false})),
  on(deleteTaskFailure, (state, {error}) =>
    ({...state, editLoading: false, editError: error})),
);
