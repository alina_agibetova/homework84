import { createAction, props } from '@ngrx/store';
import { User, UserData } from '../models/user.model';
import { CreateTask, EditStatus, EditUser, Task } from '../models/task.model';

export const fetchUsersRequest = createAction(
  '[Users] Fetch Request'
);

export const fetchUsersSuccess = createAction(
  '[Users] Fetch Success',
  props<{users: User[]}>()
);

export const fetchUsersFailure = createAction(
  '[Users] Fetch Failure',
  props<{error: string}>()
);

export const createUserRequest = createAction(
  '[Users] Create Request',
  props<{userData: UserData}>()
);
export const createUserSuccess = createAction(
  '[Users] Create Success'
);
export const createUserFailure = createAction(
  '[Users] Create Failure',
  props<{error: string}>()
);



export const fetchTasksRequest = createAction(
  '[Tasks] Fetch Request',
);

export const fetchTasksSuccess = createAction(
  '[Tasks] Fetch Success',
  props<{tasks: Task[]}>()
);

export const fetchTasksFailure = createAction(
  '[Tasks] Fetch Failure',
  props<{error: string}>()
);

export const createTaskRequest = createAction(
  '[Tasks] Create Request',
  props<{createTask: CreateTask}>()
);
export const createTaskSuccess = createAction(
  '[Tasks] Create Success'
);
export const createTaskFailure = createAction(
  '[Tasks] Create Failure',
  props<{error: string}>()
);

export const editTaskRequest = createAction(
  '[Tasks] Edit Request',
  props<{editTask: EditUser | EditStatus}>()
);
export const editTaskSuccess = createAction(
  '[Tasks] Edit Success'
);
export const editTaskFailure = createAction(
  '[Tasks] Edit Failure',
  props<{error: string}>()
);

export const deleteTaskRequest = createAction(
  '[Tasks] Delete Request',
  props<{id: string}>()
);
export const deleteTaskSuccess = createAction(
  '[Tasks] Delete Success'
);
export const deleteTaskFailure = createAction(
  '[Tasks] Delete Failure',
  props<{error: string}>()
);
