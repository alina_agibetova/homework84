import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { TodoListService } from '../services/todoList.service';
import { Router } from '@angular/router';
import {
  createTaskFailure,
  createTaskRequest,
  createTaskSuccess,
  createUserFailure,
  createUserRequest, deleteTaskFailure, deleteTaskRequest, deleteTaskSuccess,
  editTaskFailure,
  editTaskRequest,
  editTaskSuccess,
  fetchTasksFailure,
  fetchTasksRequest,
  fetchTasksSuccess,
  fetchUsersFailure,
  fetchUsersRequest,
  fetchUsersSuccess
} from './todo.actions';
import { catchError, map, mergeMap, of, tap } from 'rxjs';

@Injectable()
export class TodoEffects {
  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(() => this.todoListService.getUsers().pipe(
      map(users => fetchUsersSuccess({users})),
      catchError(() => of(fetchUsersFailure({error:'Something went wrong'})))
    ))
  ));

  createTask = createEffect(() => this.actions.pipe(
    ofType(createTaskRequest),
    mergeMap(({createTask}) => this.todoListService.addTask(createTask).pipe(
      map(() => createTaskSuccess()),
      tap(() => this.router.navigate(['/tasks'])),
      catchError(() => of(createTaskFailure({error: 'Wrong data'})))
    ))
  ));

  createUser = createEffect(() => this.actions.pipe(
    ofType(createUserRequest),
    mergeMap(({userData}) => this.todoListService.createUser(userData).pipe(
      map(() => createTaskSuccess()),
      catchError(() => of(createUserFailure({error: 'Something wrong'})))
    ))
  ));

  fetchTasks = createEffect(() => this.actions.pipe(
    ofType(fetchTasksRequest),
    mergeMap(() => this.todoListService.getTasks().pipe(
      map(tasks => fetchTasksSuccess({tasks})),
      catchError(() => of(fetchTasksFailure({error: 'Something went wrong'})))
      ))
    ));

  editTasks = createEffect(() => this.actions.pipe(
    ofType(editTaskRequest),
    mergeMap(({editTask}) => this.todoListService.editTaskById(editTask).pipe(
      map(() => editTaskSuccess()),
      catchError(() => of(editTaskFailure({error: 'Something went wrong'})))
    ))
  ));

  deleteTasks = createEffect(() => this.actions.pipe(
    ofType(deleteTaskRequest),
    mergeMap(({id}) => this.todoListService.removeTask(id).pipe(
      map(() => deleteTaskSuccess()),
      tap(() => this.router.navigate(['/tasks'])),
      catchError(() => of(deleteTaskFailure({error: 'Something went wrong'})))
    ))
  ));


  constructor(private actions: Actions,
              private todoListService: TodoListService,
              private router: Router) {
  }
}


