import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { Task } from '../models/task.model';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { ActivatedRoute } from '@angular/router';
import { deleteTaskRequest, editTaskRequest, fetchTasksRequest, fetchUsersRequest } from '../store/todo.actions';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.sass']
})
export class TodoComponent implements OnInit {
  users: Observable<User[]>;
  tasks: Observable<Task[]>;
  loading: Observable<boolean>;
  error: Observable<null | string>


  constructor(private store: Store<AppState>,
              private route: ActivatedRoute) {
    this.users = store.select(state => state.users.users);
    this.loading = store.select(state => state.users.fetchLoading);
    this.error = store.select(state => state.users.fetchError);

    this.tasks = store.select(state => state.tasks.tasks);
    this.loading = store.select(state => state.tasks.fetchLoading);
    this.error = store.select(state => state.tasks.fetchError);
  }



  ngOnInit(): void {
    this.store.dispatch(fetchTasksRequest());
    this.store.dispatch(fetchUsersRequest());
  }

  onChange(id: string, event:Event) {
    const value = <HTMLSelectElement>(event.target);
    console.log(value.value);
    const editDataUser = {
      _id: id,
      user: value.value
    }
      this.store.dispatch(editTaskRequest({editTask: editDataUser}));
  }

  onChangeStatus(id: string, event:Event){
    const value = <HTMLSelectElement>(event.target);
    const editDataStatus = {
      _id: id,
      status: value.value,
    }
    this.store.dispatch(editTaskRequest({editTask: editDataStatus }));
  }

  onDelete(id: string) {
    console.log(id)
    this.store.dispatch(deleteTaskRequest({id}))
  }
}
