import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../store/types';
import { CreateTask, Task } from '../models/task.model';
import { createTaskRequest, fetchUsersRequest } from '../store/todo.actions';
import { User } from '../models/user.model';


@Component({
  selector: 'app-new-todo',
  templateUrl: './new-todo.component.html',
  styleUrls: ['./new-todo.component.sass']
})
export class NewTodoComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  users: Observable<User[]>;
  tasks: Observable<Task[]>;
  loading: Observable<boolean>;
  error: Observable<string | null>;
  constructor(private store: Store<AppState>) {
    this.loading = store.select(state => state.tasks.createLoading);
    this.error = store.select(state => state.tasks.createError);
    this.tasks = store.select(state => state.tasks.tasks);
    this.users = store.select(state => state.users.users);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchUsersRequest());
  }

  onSubmit() {
    const createData: CreateTask = {
      title: this.form.value.title,
      user: this.form.value.user
    };

    this.store.dispatch(createTaskRequest({createTask: createData}));

  }

}
