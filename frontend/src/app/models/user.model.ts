export class User {
  constructor(
    public _id: string,
    public username: string
  ) {}
}

export interface UserData {
  _id: string,
  username: string
}

export interface ApiUserData {
  _id: string,
  username: string
}
