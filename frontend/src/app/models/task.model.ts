export class Task {
  constructor(
    public _id: string,
    public user: {
      _id: string,
      username: string
    },
    public title: string,
    public status: string
  ) {}
}

export interface EditUser {
  _id: string,
  user: string,
}

export interface EditStatus {
  _id: string,
  status: string,
}

export interface ApiTaskData {
  _id: string,
  user: {
    _id: string,
    username: string,
  },
  title: string,
  status: string
}

export interface CreateTask {
  title: string,
  user: string
}
