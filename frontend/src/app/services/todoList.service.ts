import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiUserData, User, UserData } from '../models/user.model';
import { map } from 'rxjs';
import { ApiTaskData, CreateTask, EditStatus, EditUser, Task } from '../models/task.model';

@Injectable({
  providedIn: 'root'
})
export class TodoListService {

  constructor(private http: HttpClient) { }

  getUsers() {
    return this.http.get<ApiUserData[]>('http://localhost:8000/users').pipe(
      map( response => {
        return response.map(userData => {
          return new User(
            userData._id,
            userData.username
          );
        });
      })
    )
  }

  getTasks() {
    return this.http.get<ApiTaskData[]>('http://localhost:8000/tasks').pipe(
      map( response => {
        return response.map(taskData => {
          return new Task(
            taskData._id,
            taskData.user,
            taskData.title,
            taskData.status
          );
        });
      })
    )
  }

  addTask(createTask: CreateTask){
    const body = {
      title: createTask.title,
      user: createTask.user
    }
    return this.http.post('http://localhost:8000/tasks', body);
  }

  createUser(userData: UserData){
    return this.http.post('http://localhost:8000/users', userData);
  }

  editTaskById(editTask: EditUser | EditStatus){
    return this.http.put(`http://localhost:8000/tasks/${editTask._id}`, editTask);
  }

  removeTask(id: string){
    console.log(id)
    return this.http.delete(`http://localhost:8000/tasks/${id}`);
  }

}
