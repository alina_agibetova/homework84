const mongoose = require('mongoose');
const config = require("./config");
const Task = require('./models/Task');
const User = require('./models/User');

const run = async () => {
  await mongoose.connect(config.mongo.db, config.mongo.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections){
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [alina, jane] = await User.create({
    username: 'Alina'
  }, {
    username: 'Jane'
  });

  await Task.create({
    user: alina,
    title: 'Add homework',
    status: 'done',
  }, {
    user: jane,
    title: 'Something doing',
    status: 'in progress',
  });

  await mongoose.connection.close();
}

run().catch(e => console.error(e))