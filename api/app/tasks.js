const express = require('express');
const Task = require('../models/Task');

const router = express.Router();

router.get('/', async (req, res, next) => {
  try{
    const tasks = await Task.find().populate('user', 'username');
    return res.send(tasks);
  } catch (e) {
    next(e);
  }

});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.title){
      return res.send(400).send({message: 'Are required'});
    }
      const taskData = {
        user: req.body.user,
        title: req.body.title,
        status: req.body.status,
      };
    const task = new Task(taskData);
    await task.save();
    return res.send({message: 'Created new task', id: task._id});
  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    if (req.body.user){
      console.log(req.body.user)
      const taskId = await Task.updateOne({_id: req.params.id}, {user: req.body.user});
      return res.send({message: 'Changed', taskId});
    }

    if (req.body.status){
      const taskId = await Task.updateOne({_id: req.params.id}, {status: req.body.status});
      return res.send({message: 'Changed', taskId});
    }
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try {
      const task = await Task.deleteOne({_id: req.params.id});
      return res.send(task);
  } catch (e) {
    next(e);
  }
});

module.exports = router;